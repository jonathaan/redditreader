package us.theappacademy.redditreader;

import android.support.v4.app.Fragment;

public class RedditActivity extends singleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new RedditListFragment();
    }
}
