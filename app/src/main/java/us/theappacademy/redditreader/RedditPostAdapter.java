package us.theappacademy.redditreader;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class RedditPostAdapter extends RecyclerView.Adapter<RedditPostAdapter> {
    private String[] redditPosts;

    public RedditPostAdapter(String[] redditPosts){
        this.redditPosts = redditPosts;
    }

    @Override
    public RedditPostAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.Layout.simple_list_item_1, parent, false);
        return new RedditPostHolder(view);
    }

    @Override
    public void onBindViewHolder(RedditPostAdapter holder, int position) {
        holder.titleText.setText(redditPosts[position]);
    }

    @Override
    public int getItemCount() {
        return redditPosts.length;
    }
}
